package healthyJavaBean;

public class MyAdvise {
	private int aid;
	private int sid;
	private int did;
	private int gid;
	private String context;
	private String time;
	private String name;//医生或者监护人的姓名
	public MyAdvise(){}
	public void setaid(int aid)
	{
		this.aid=aid;
	}
	public int getaid()
	{
		return this.aid;
	}
	
	public void setsid(int sid)
	{
		this.sid=sid;
	}
	public int getsid()
	{
		return this.sid;
	}
	
	public void setdid(int did)
	{
		this.did=did;
	}
	public int getdid()
	{
		return this.did;
	}
	
	public void setgid(int gid)
	{
		this.gid=gid;
	}
	public int getgid()
	{
		return this.gid;
	}
	
	public void setcontext(String context)
	{
		this.context=context;
	}
	public String getcontext()
	{
		return this.context;
	}
	
	public void settime(String time)
	{
		this.time=time;
	}
	public String gettime()
	{
		return this.time;
	}
	
	public void setname(String name)
	{
		this.name=name;
	}
	public String getname()
	{
		return this.name;
	}
	}
