package healthyJavaBean;

public class MyPatient {
	private int pid;
	private String password;
	private String name;
	private int sex;
	private int age;
	private String location;
	private String phone;
	private String gphone;
	private int did;
	private int gid;
	public MyPatient()
	{}
	public void setpid(int pid)
	{
		this.pid=pid;
	}
	public int getpid()
	{
		return this.pid;
	}
	
	public void setpassword(String password)
	{
		this.password=password;
	}
	public String getpassword()
	{
		return this.password;
	}
	
	public void setname(String name)
	{
		this.name=name;
	}
	public String getname()
	{
		return this.name;
	}
	
	public void setsex(int sex)
	{
		this.sex=sex;
	}
	public int getsex()
	{
		return this.sex;
	}
	
	public void setage(int age)
	{
		this.age=age;
	}
	public int getage()
	{
		return this.age;
	}
	
	public void setlocation(String location)
	{
		this.location=location;
	}
	public String getlocation()
	{
		return this.location;
	}
	
	public void setphone(String phone)
	{
		this.phone=phone;
	}
	public String getphone()
	{
		return this.phone;
	}
	
	public void setgphone(String gphone)
	{
		this.gphone=gphone;
	}
	public String getgphone()
	{
		return this.gphone;
	}
	
	public void setdid(int did)
	{
		this.did=did;
	}
	public int getdid()
	{
		return this.did;
	}
	
	public void setgid(int gid)
	{
		this.gid=gid;
	}
	public int getgid()
	{
		return this.gid;
	}
}
