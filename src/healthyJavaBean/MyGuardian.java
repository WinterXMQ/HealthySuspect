package healthyJavaBean;

public class MyGuardian {
	private int gid;
	private String password;
	private String image;
	private String name;
	private int sex;
	private int age;
	private String gphone;
	private String location;
	public MyGuardian()
	{}
	
	public void setgid(int gid)
	{
		this.gid=gid;
	}
	public int getgid()
	{
		return this.gid;
	}
	
	public void setsex(int sex)
	{
		this.sex=sex;
	}
	public int getsex()
	{
		return this.sex;
	}
	
	public void setage(int age)
	{
		this.age=age;
	}
	public int getage()
	{
		return this.age;
	}
	
	public void setpassword(String password)
	{
		this.password=password;
	}
	public String getpassword()
	{
		return this.password;
	}
	
	public void setimage(String image)
	{
		this.image=image;
	}
	public String getimage()
	{
		return this.image;
	}
	
	public void setname(String name)
	{
		this.name=name;
	}
	public String getname()
	{
		return this.name;
	}
	
	public void setgphone(String gphone)
	{
		this.gphone=gphone;
	}
	public String getgphone()
	{
		return this.gphone;
	}
	
	public void setlocation(String location)
	{
		this.location=location;
	}
	public String getlocation()
	{
		return this.location;
	}
}
