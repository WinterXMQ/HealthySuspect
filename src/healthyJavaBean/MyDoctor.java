package healthyJavaBean;

public class MyDoctor {private int did;
private String password;
private String image;
private String name;
private int sex;
private int age;
private String phone;
private String organization;
private String title;//职称
private String workingtime;
private String professional;
public MyDoctor()
{
}
public void setdid(int did)
{
	this.did=did;
}
public int getdid()
{
	return this.did;
}

public void setsex(int sex)
{
	this.sex=sex;
}
public int getsex()
{
	return this.sex;
}

public void setage(int age)
{
	this.age=age;
}
public int getage()
{
	return this.age;
}

public void setimage(String image)
{
	this.image=image;
}
public String getimage()
{
	return this.image;
}

public void setphone(String phone)
{
	this.phone=phone;
}
public String getphone()
{
	return this.phone;
}

public void setname(String name)
{
	this.name=name;
}
public String getname()
{
	return this.name;
}

public void setpassword(String password)
{
	this.password=password;
}
public String getpassword()
{
	return this.password;
}

public void setorganization(String organization)
{
	this.organization=organization;
}
public String getorganization()
{
	return this.organization;
}

public void settitle(String title)
{
	this.title=title;
}
public String gettitle()
{
	return this.title;
}

public void setworkingtime(String workingtime)
{
	this.workingtime=workingtime;
}
public String getworkingtime()
{
	return this.workingtime;
}

public void setprofessional(String professional)
{
	this.professional=professional;
}
public String getprofessional()
{
	return this.professional;
}}
