package healthyJavaBean;

public class MyState {
	private int sid;
	private int pid;
	private int ecg;
	private float temperature;
	private int pulse;
	private int pressure1;
	private int pressure2;
	private int bloodoxygen;
	private int state;
	private String time;
	
	public MyState(){}
	public void setsid(int sid)
	{
		this.sid=sid;
	}
	public int getsid()
	{
		return this.sid;
	}
	
	public void setpid(int pid)
	{
		this.pid=pid;
	}
	public int getpid()
	{
		return this.pid;
	}
	
	public void setecg(int ecg)
	{
		this.ecg=ecg;
	}
	public int getecg()
	{
		return this.ecg;
	}
	
	public void settemperature(float temperature)
	{
		this.temperature=temperature;
	}
	public float gettemperature()
	{
		return this.temperature;
	}
	
	public void setpulse(int pulse)
	{
		this.pulse=pulse;
	}
	public int getpulse()
	{
		return this.pulse;
	}
	
	public void setpressure1(int pressure1)
	{
		this.pressure1=pressure1;
	}
	public int getpressure1()
	{
		return this.pressure1;
	}
	
	public void setpressure2(int pressure2)
	{
		this.pressure2=pressure2;
	}
	public int getpressure2()
	{
		return this.pressure2;
	}
	
	public void setbloodoxygen(int bloodoxygen)
	{
		this.bloodoxygen=bloodoxygen;
	}
	public int getbloodoxygen()
	{
		return this.bloodoxygen;
	}
	
	public void setstate(int state)
	{
		this.state=state;
	}
	public int getstate()
	{
		return this.state;
	}
	
	public void settime(String time)
	{
		this.time=time;
	}
	public String gettime()
	{
		return this.time;
	}
}
