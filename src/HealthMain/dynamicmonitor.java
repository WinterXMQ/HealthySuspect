package HealthMain;

import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import healthyTool.SerialCommon;
import healthyTool.ServerData;
import healthyTool.ServerHelp;
import healthyTool.maibo_util;
import healthyTool.tiwen_util;
import healthyTool.xindian_util;
import healthyTool.xueya_util;
import healthyTool.xueyang_util;

import java.util.Timer;

public class dynamicmonitor {

	// 定义所需的组件
	// 心电模块
	int ECGCount;
	int ECGHigh;
	xindian_util xdian;

	// 体温
	double temperature_Count = 0;
	int temperature_Data = 0;
	tiwen_util tiwen;
	

	public int pid;

	// 血压
	Boolean bloodFlag;
	
	xueya_util xueya;

	// 血氧
	int SpO2_Count = 0;
	int SpO2_Data = 0;
	
	xueyang_util xueyang;

	// 脉搏
	int pluse_Count = 0;
	int pluse_Data = 0;
	maibo_util maibo;

	
	// 定义串口工具
	SerialCommon sc;
	// 定义数据发送类
	ServerHelp seversend;

	Timer time;

	public dynamicmonitor(int pid) {
		this.pid = pid;
		// 实例化串口
		sc = new SerialCommon();
		System.out.println("打开串口");
		sc.OpenCom();
		System.out.println("已经 打开串口**************************");

		xdian = new xindian_util();
		tiwen = new tiwen_util(34.0f);
		xueya = new xueya_util(0, 0);
		xueyang = new xueyang_util(100);
		maibo = new maibo_util();
		
		new Thread(new Runnable() {
			public void run() {
				try {
					temperature_Count = 1;
					temperature_Data = 0;

					bloodFlag = true;

					SpO2_Data = 0;
					SpO2_Count = 1;

					pluse_Data = 0;
					pluse_Count = 1;

					ECGCount = 0;
					ECGHigh = 0;

					while (true) {

						if (sc.inputStream.available() > 0) {

							Thread.sleep(1000);

							System.out
									.println("****************************++++++++++++++++++++++++++++");
							System.out
									.println("****************************++++++++++++++++++++++++++++");
							System.out
									.println("****************************++++++++++++++++++++++++++++");
							int[] ECG = new int[10];

							ECG = ECGWave(sc.ECG);

							double temperature = sc.temperature;
							System.out.println("温度值为 " + temperature
									+ "**************************");

							// 开启温度检测模块***********************************
							if (temperature > 34.8 && temperature < 42) {
								
								 tiwen.myaction((float)(temperature));

							}
							if (temperature > 36 && temperature < 40) {
								temperature_Data += temperature;
								temperature_Count++;
							}

							// 开启血压检测模块************************************
							int[] BooldPress = new int[2];
							BooldPress = sc.BloodPressData;
							if (sc.BloodPressData[0] > 0
									&& sc.BloodPressData[1] > 0
									&& bloodFlag == true) {

								xueya.h1 = BooldPress[0];
								xueya.h2 = BooldPress[1];
								// jp3.startPress();
								bloodFlag = false;
							}

							// 开启血氧、脉搏检测模块*************************************
							int[] OXY = new int[2];
							OXY = sc.OXYdata;
							//
							xueyang.myaction(OXY[0]);
							if (OXY[0] > 95 && OXY[0] < 100) {
								SpO2_Data += OXY[0];
								SpO2_Count++;
							}

							
							if (OXY[1] > 60 && OXY[1] < 80) {
								pluse_Data += OXY[1];
								pluse_Count++;
								
							}

							if (timerpanel.flag == true) {
								jp5.EndAction();
								System.out.println("timerpanel is true!!!");
								double temperatureFinal = 0.0;
								if (temperature_Count != 1) {
									temperatureFinal = temperature_Data
											/ (temperature_Count - 1);
								}
								int SpO2Final = 0;
								if (SpO2_Count != 1) {
									SpO2Final = SpO2_Data / (SpO2_Count - 1);
								}
								int pluseFinal = 0;
								if (pluse_Count != 1) {
									pluseFinal = pluse_Data / pluse_Count;
								}
								int ECGFinal = ECGHigh / timerpanel.time_Count
										+ 20;
								SimpleDateFormat sdf = new SimpleDateFormat(
										"yyyy-MM-dd hh:mm:ss");
								Date time = new Date();
								String nowTime = sdf.format(time);
								java.sql.Timestamp tt = new java.sql.Timestamp(
										time.getTime());
								System.out.println(tt.toString());

								System.out.println("ECGFinal is " + ECGFinal);
								if (ECGFinal < 30) {
									ECGFinal = 0;
								}

								// 标志位，用于判断是否是单个测量
								int f_p = 0;
								int f_t = 0;
								int f_h = 0;
								int f_s = 0;
								int f_e = 0;
								int[] Theflag = new int[5];
								int FlagServer = 0;
								// 初始化
								for (int i = 0; i < Theflag.length; i++) {
									Theflag[i] = 1;
								}
								if (pluseFinal == 0) {
									Theflag[0] = 0;
								}
								if (temperatureFinal == 0) {
									Theflag[1] = 0;
								}
								if (jp3.h1 == 0) {
									Theflag[2] = 0;
								}
								if (SpO2Final == 0) {
									Theflag[3] = 0;
								}
								if (ECGFinal == 0) {
									Theflag[4] = 0;
								}
								for (int i = 0; i < Theflag.length; i++) {
									if (Theflag[i] == 0) {// 当有一个没有测试的时候跳出
										FlagServer = 1;
										break;
									}
								}

								sc.CloseCom();
								System.out.println("心电记录总次数为：" + ECGCount);
								System.out.println("心电跳动总次数为：" + ECGHigh);

								System.out.println("关闭串口");
								break;
							}
						}
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}).start();
	}

	// ECG波形处理函数
	public int[] ECGWave(int[] ECG) {
		ECGCount++;
		Boolean ECGflag = true;// 小波控制位
		Boolean ECGBflag = true;// 大波控制
		int[] ECGWave = new int[10];
		for (int i = 0; i < 10; i++) {
			ECGWave[i] = ECG[i];
			if (ECGWave[i] > 2500) {
				ECGWave[i] = 2500;
			}
			if (ECGWave[i] < 1500) {
				ECGWave[i] = 1500;
			}
			int temp = ECG[i] + 5;
			temp = temp / 10;
			ECGWave[i] = ECGWave[i] / 10;
			if (temp > ECGWave[i]) {
				ECGWave[i]++;
			}
			// int temp2 = (ECGWave[i] + 5) / 10;
			// ECGWave[i] = ECGWave[i] / 10;
			// if(ECGWave[i] < temp2){
			// ECGWave[i] = temp2 * 10;
			// }else{
			// ECGWave[i] = ECGWave[i] / 10 * 10;
			// }
			// System.out.print(ECGWave[i]+" ");
		}
		for (int k = 0; k < 10; k++) {
			if (ECGWave[k] > 200 && ECGWave[k] < 210) {
				if (ECGflag == true) {
					ECGWave[k] = 205;
					ECGflag = false;
				} else {
					ECGWave[k] = 200;
					ECGflag = true;
				}
			}
			if (ECGWave[k] > 210) {
				if (ECGBflag == true) {
					ECGWave[k] = 230;
					ECGHigh++;
					ECGBflag = false;
				} else {
					if (ECGflag == true) {
						ECGWave[k] = 205;
					} else {
						ECGWave[k] = 200;
					}
				}
			}
			if (ECGWave[k] < 200) {
				if (ECGflag == false) {
					ECGWave[k] = 195;
					ECGflag = true;
				} else {
					ECGWave[k] = 205;
					ECGflag = false;
				}
			}
		}
		System.out.println();
		return ECGWave;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
