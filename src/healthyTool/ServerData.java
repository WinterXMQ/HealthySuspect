package healthyTool;

import healthyJavaBean.MyAdvise;
import healthyJavaBean.MyDoctor;
import healthyJavaBean.MyGuardian;
import healthyJavaBean.MyPatient;
import healthyJavaBean.MyState;

import java.util.ArrayList;

public class ServerData {
	//根据sid得到服务器数据的中对应sid的mystate数据
	public static MyState getMyState(int sid,int pid)
	{
		try {
			return ServerHelp.getMyState_ServerHelp(sid, pid);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	//众多数据得到state值
	//其中1:健康
	//   2:良好
	//   3:一般
	//   4:较差
	//   5：危险
		public static int SetState(int pid,int ecg,float t,int pulse,int p1,int p2,int b,String time)
		{
			try {
				return ServerHelp.setState_ServerHelp(pid,ecg,t,pulse,p1,p2,b,time);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return 5;
		}
	//根据pid和password得到服务器中对应的pid
	public static int getMyPid(int pid,String password)
	{
		try {
			return ServerHelp.getMyPid_ServerHelp(pid,password);
		} catch (Exception e) {
				// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	//根据pid得到服务器中对应的pid的mypatient数据
	public static MyPatient getMyPatient(int pid)
	{
		try {
			return ServerHelp.getMyPatient_ServerHelp(pid);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	//根据pid得到所有的sid
	public static ArrayList<MyState> getAllMyState(int pid)
	{
		try {
			return ServerHelp.getAllMyState_ServerHelp(pid);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	//根据pid得到对应的doctor
	public static MyDoctor getMyDoctor(int did)
	{
		try {
			return ServerHelp.getMyDoctor_ServerHelp(did);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	//根据gid得到对应的guardian
		public static MyGuardian getMyGuardian(int gid)
		{
			try {
				return ServerHelp.getMyGuardian_ServerHelp(gid);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		
	//根据sid得到所有的advise
	public static ArrayList<MyAdvise> getAllMyadvise(int sid)
	{
		try {
			return ServerHelp.getAllMyAdvise_ServerHelp(sid);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
