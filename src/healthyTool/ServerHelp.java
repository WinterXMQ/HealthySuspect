package healthyTool;



import healthyJavaBean.MyAdvise;
import healthyJavaBean.MyDoctor;
import healthyJavaBean.MyGuardian;
import healthyJavaBean.MyPatient;
import healthyJavaBean.MyState;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.Thread.State;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class ServerHelp {
	public static String SERVER_URL = "http://debian.winterxmq.me:8080/MyInter/";
//    public static void main(String[] args) {
//        
//        String json = loadJSON(SERVER_URL);
//        System.out.println(json);
//    }
    
    //根据url返回json格式的字符创
    public static String loadJSON (String url) {
        StringBuilder json = new StringBuilder();
        try {
            URL oracle = new URL(url);
            URLConnection yc = oracle.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream(),"UTF-8"));
            String inputLine = null;
            while ( (inputLine = in.readLine()) != null) {
                json.append(inputLine);
            }
            in.close();
        } catch (MalformedURLException e) {
        } catch (IOException e) {
        }
        //System.out.println(json.toString());
        return json.toString();
    }
  //根据pid和password得到pid
    public static int getMyPid_ServerHelp(int pid,String password) throws Exception
    {
    	String url=SERVER_URL+"LogForPatient"+"?id="+pid+"&password="+password;
		System.out.println(url);
		String json=loadJSON(url);
		return parseJSON_GetResult(json);
    }
    //根据众多数据得到状态值state
    public static int setState_ServerHelp(int pid,int ecg,float t,int pulse,int p1,int p2,int b,String time) throws Exception
    {
    	String url=SERVER_URL+"SetState?pid="+pid+"&ecg="+ecg+"&temperature="+t+"&pulse="+pulse+"&pressure1="+p1+"&pressure2="+p2
    			+"&bloodoxygen="+b+"&time="+time.substring(0,10)+"%20"+time.substring(11);
		System.out.println(url);
		String json=loadJSON(url);
		return parseJSON_GetResult(json);
    }
    //根据sid和pid得到state
    public static MyState getMyState_ServerHelp(int sid,int pid) throws Exception
    {
    	String url=SERVER_URL+"GetState"+"?sid="+sid+"&pid="+pid;
		System.out.println(url);
		String json=loadJSON(url);
		return parseJSON_GetState(json).get(0);
    }
    
    //得到pid对应的patient
    public static MyPatient getMyPatient_ServerHelp(int pid) throws Exception
    {
    	String url=SERVER_URL+"GetPatient"+"?id="+pid+"&model="+1;
		System.out.println(url);
		String json=loadJSON(url);
		return parseJSON_GetPatient(json).get(0);
    }
    //得到pid对应的所有的state
    public static ArrayList<MyState> getAllMyState_ServerHelp(int pid) throws Exception
    {
    	String url=SERVER_URL+"GetState"+"?sid=-1"+"&pid="+pid;
		System.out.println(url);
		String json=loadJSON(url);
		return parseJSON_GetState(json);
    }
    //得到did对应的自己的doctor
    public static MyDoctor getMyDoctor_ServerHelp(int did) throws Exception
    {
    	String url=SERVER_URL+"GetDoctor"+"?did="+did;
		System.out.println(url);
		String json=loadJSON(url);
		return parseJSON_GetDoctor(json).get(0);
    }
  //得到gid对应的自己的guardian
    public static MyGuardian getMyGuardian_ServerHelp(int gid) throws Exception
    {
    	String url=SERVER_URL+"GetGuardian"+"?gid="+gid;
		System.out.println(url);
		String json=loadJSON(url);
		return parseJSON_GetGuardian(json).get(0);
    }
    //解析state
    private static ArrayList<MyState> parseJSON_GetState(String json)throws Exception{//解析json
		ArrayList newses=new ArrayList();
		JSONObject jsonobject=new JSONObject();
		JSONArray array=jsonobject.fromObject(json).getJSONArray("list");
		for (int i = 0; i < array.size(); i++) {
			JSONObject jsonitem=array.getJSONObject(i);
			MyState mystate=new MyState();
			mystate.setsid(jsonitem.getInt("sid"));
			mystate.setpid(jsonitem.getInt("pid"));
			mystate.setecg(jsonitem.getInt("ecg"));
			mystate.settemperature(jsonitem.getInt("temperature"));
			mystate.setpulse(jsonitem.getInt("pulse"));
			mystate.setpressure1(jsonitem.getInt("pressure1"));
			mystate.setpressure2(jsonitem.getInt("pressure2"));
			mystate.setbloodoxygen(jsonitem.getInt("bloodoxygen"));
			mystate.setstate(jsonitem.getInt("state"));
			mystate.settime(jsonitem.getString("time"));		
			newses.add(mystate);
		}
		return newses;
	}
    //解析patient
    private static ArrayList<MyPatient> parseJSON_GetPatient(String json)throws Exception{//解析json
		ArrayList newses=new ArrayList();
		JSONObject jsonobject=new JSONObject();
		JSONArray array=jsonobject.fromObject(json).getJSONArray("list");
		for (int i = 0; i < array.size(); i++) {
			JSONObject jsonitem=array.getJSONObject(i);
			MyPatient myPatient=new MyPatient();
			myPatient.setpid(jsonitem.getInt("pid"));
			myPatient.setpassword(jsonitem.getString("password"));
			myPatient.setname(jsonitem.getString("name"));
			myPatient.setsex(jsonitem.getInt("sex"));
			myPatient.setage(jsonitem.getInt("age"));
			myPatient.setlocation(jsonitem.getString("location"));
			myPatient.setphone(jsonitem.getString("phone"));
			myPatient.setgphone(jsonitem.getString("gphone"));
			myPatient.setdid(jsonitem.getInt("did"));
			myPatient.setgid(jsonitem.getInt("gid"));
			newses.add(myPatient);
		}
		return newses;
	}
    //解析doctor
    private static ArrayList<MyDoctor> parseJSON_GetDoctor(String json)throws Exception{//解析json
		ArrayList newses=new ArrayList();
		JSONObject jsonobject=new JSONObject();
		JSONArray array=jsonobject.fromObject(json).getJSONArray("list");
		for (int i = 0; i < array.size(); i++) {
			JSONObject jsonitem=array.getJSONObject(i);
			MyDoctor md=new MyDoctor();
			md.setdid(jsonitem.getInt("did"));
			md.setpassword(jsonitem.getString("password"));
			md.setname(jsonitem.getString("name"));
			md.setsex(jsonitem.getInt("sex"));
			md.setage(jsonitem.getInt("age"));
			md.setphone(jsonitem.getString("phone"));
			md.setorganization(jsonitem.getString("organization"));
			md.settitle(jsonitem.getString("title"));
			md.setworkingtime(jsonitem.getString("workingtime"));
			md.setprofessional(jsonitem.getString("professional"));
			newses.add(md);
		}
		return newses;
	}
    
  //解析guardian
    private static ArrayList<MyGuardian> parseJSON_GetGuardian(String json)throws Exception{//解析json
		ArrayList newses=new ArrayList();
		JSONObject jsonobject=new JSONObject();
		JSONArray array=jsonobject.fromObject(json).getJSONArray("list");
		for (int i = 0; i < array.size(); i++) {
			JSONObject jsonitem=array.getJSONObject(i);
			MyGuardian mg=new MyGuardian();
			mg.setgid(jsonitem.getInt("gid"));
			mg.setpassword(jsonitem.getString("password"));
			mg.setname(jsonitem.getString("name"));
			mg.setsex(jsonitem.getInt("sex"));
			mg.setage(jsonitem.getInt("age"));
			mg.setgphone(jsonitem.getString("gphone"));
			mg.setlocation(jsonitem.getString("location"));
			newses.add(mg);
		}
		return newses;
	}
    
    //根据sid得到advise
    public static ArrayList<MyAdvise> getAllMyAdvise_ServerHelp(int sid) throws Exception
    {
    	String url=SERVER_URL+"GetAdvise"+"?sid="+sid;
		System.out.println(url);
		String json=loadJSON(url);
		return parseJSON_GetAdvise(json);
    }
    //解析advise
    private static ArrayList<MyAdvise> parseJSON_GetAdvise(String json)throws Exception{//解析json
		ArrayList newses=new ArrayList();
		JSONObject jsonobject=new JSONObject();
		JSONArray array=jsonobject.fromObject(json).getJSONArray("list");
		for (int i = 0; i < array.size(); i++) {
			JSONObject jsonitem=array.getJSONObject(i);
			MyAdvise myAdvise=new MyAdvise();
			myAdvise.setaid(jsonitem.getInt("aid"));
			myAdvise.setsid(jsonitem.getInt("sid"));
			myAdvise.setdid(jsonitem.getInt("did"));
			myAdvise.setgid(jsonitem.getInt("gid"));
			myAdvise.setcontext(jsonitem.getString("context"));
			myAdvise.settime(jsonitem.getString("time"));
			myAdvise.setname(jsonitem.getString("name"));
			newses.add(myAdvise);
		}
		return newses;
	}
    
    
    //解析简单结果
    private static int parseJSON_GetResult(String json)
    {
    	JSONObject jsonObject=new JSONObject();
    	int result=jsonObject.fromObject(json).getInt("result");
    	return result;
    }
}
